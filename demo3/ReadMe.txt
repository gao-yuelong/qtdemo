基于某一个模板窗体派生出多样化的子类窗体

BaseWidget是一个模板窗体，有两个按钮，和一个Widget，派生子类可以在Widget上添加任意控件（见basewidget.ui）。

FirstWidget在BaseWidget基础上添加一个QLabel
SecondWidget在BaseWidget基础上添加一个QPushButton

需要注意的是要将BaseWidget的ui指针定义为受保护的
protected:
    Ui::BaseWidget *ui;
同时在用到BaseWidget中控件的时候引入#include "ui_basewidget.h"头文件。