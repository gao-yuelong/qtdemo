#include "basewidget.h"
#include <QApplication>

#include "firstwidget.h"
#include "secondwidget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //BaseWidget w;
    //w.show();

    //FirstWidget w;
    //w.show();

    SecondWidget w;
    w.show();

    return a.exec();
}
