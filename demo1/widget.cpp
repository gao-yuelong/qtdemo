#include "widget.h"
#include <QBitmap>
#include <QPainter>
#include <QMouseEvent>
#include <QJsonObject>
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QPixmap pixmap;
    pixmap.load(":/new/res/bocai.png");
    resize(pixmap.size());//重绘窗体大小
    setMask(pixmap.mask());//设置遮罩效果
    //QPixmap的mask函数获取图片自身的遮罩
    //图片的透明部分实际上是一个遮罩

    m_plbLogin = new QLabel();
    m_pixLogin.load(":/new/res/登录.png");
    m_plbLogin->setPixmap(m_pixLogin);

    m_plbUserName = new QLabel();
    m_pixUserName.load(":/new/res/username.png");
    m_plbUserName->setPixmap(m_pixUserName);

    m_plbPassWord = new QLabel();
    m_pixPassWord.load(":/new/res/password.png");
    m_plbPassWord->setPixmap(m_pixPassWord);

    m_pleUserName = new QLineEdit();
    m_pleUserName->setPlaceholderText("6 <= length <= 10");
    m_pleUserName->setFixedWidth(200);

    m_plePassWord = new QLineEdit();
    m_plePassWord->setFixedWidth(200);
    m_plePassWord->setEchoMode(QLineEdit::Password);

    m_btnOk = new QPushButton(tr("Sign in"));
    m_btnOk->setFixedWidth(200);
    m_btnOk->setFixedHeight(50);

    m_qhbox1 = new QHBoxLayout();
    m_qhbox2 = new QHBoxLayout();
    m_qhbox3 = new QHBoxLayout();
    m_qhbox4 = new QHBoxLayout();

    m_qhbox1->addStretch();
    m_qhbox1->addWidget(m_plbLogin);
    m_qhbox1->addStretch();

    m_qhbox2->addStretch();
    m_qhbox2->addWidget(m_plbUserName);
    m_qhbox2->addWidget(m_pleUserName);
    m_qhbox2->addStretch();

    m_qhbox3->addStretch();
    m_qhbox3->addWidget(m_plbPassWord);
    m_qhbox3->addWidget(m_plePassWord);
    m_qhbox3->addStretch();

    m_qhbox4->addStretch();
    m_qhbox4->addWidget(m_btnOk);
    m_qhbox4->addStretch();

    m_qvbox = new QVBoxLayout();

    m_qvbox->addStretch();
    m_qvbox->addItem(m_qhbox1);
    m_qvbox->addStretch();
    m_qvbox->addItem(m_qhbox2);
    m_qvbox->addSpacing(20);
    m_qvbox->addItem(m_qhbox3);
    m_qvbox->addSpacing(30);
    m_qvbox->addItem(m_qhbox4);
    m_qvbox->addStretch();
    setLayout(m_qvbox);

    connect(m_btnOk, SIGNAL(clicked()), this, SLOT(onClickYes()));
    connect(m_pleUserName, SIGNAL(textChanged(const QString &)), this, SLOT(userTextChange(const QString &)));

}

void Widget::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.drawPixmap(0, 0, QPixmap(":/new/res/bocai.png"));
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        //鼠标左键点击，记下当前点击位置相对于窗体左上角偏移值
        dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
    if (event->button() == Qt::RightButton) {
        close();//鼠标右键点击退出
    }
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        //move函数的参数指窗体左上角的位置
        //因此用鼠标当前位置的值减去偏移值
        move(event->globalPos() - dragPosition);
        event->accept();
    }
}

void Widget::userTextChange(const QString &text)
{
    if (text.length() >= 6 && text.length() <= 10) {
        m_pleUserName->setStyleSheet("QLineEdit {border : 2px solid green;}");
    } else {
        m_pleUserName->setStyleSheet("QLineEdit {border : 2px solid red;}");
    }
}

void Widget::onClickYes()
{
    QJsonObject *pjsonObj = new QJsonObject();
    QString strName = m_pleUserName->text();
    QString strPass = m_plePassWord->text();
    pjsonObj->insert("username:", strName);
    pjsonObj->insert("password:", strPass);
    QVariantHash qvarhash = pjsonObj->toVariantHash();
    qDebug() << qvarhash;
    qDebug() << qvarhash.value("username:");
    qDebug() << qvarhash.value("password:");
}

Widget::~Widget()
{

}
