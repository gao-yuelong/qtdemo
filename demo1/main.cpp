#include "widget.h"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    QFile qss(":/new/res/qss.qss");
    qss.open(QFile::ReadOnly);
    w.setStyleSheet(qss.readAll());
    w.show();

    return a.exec();
}
