#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

private:
    QLabel *m_plbLogin;
    QLabel *m_plbUserName;
    QLabel *m_plbPassWord;

    QLineEdit *m_pleUserName;
    QLineEdit *m_plePassWord;

    QPushButton *m_btnOk;

    QHBoxLayout *m_qhbox1;
    QHBoxLayout *m_qhbox2;
    QHBoxLayout *m_qhbox3;
    QHBoxLayout *m_qhbox4;

    QVBoxLayout *m_qvbox;

    QPixmap m_pixLogin;
    QPixmap m_pixUserName;
    QPixmap m_pixPassWord;

    QPoint dragPosition;

private slots:
    void userTextChange(const QString &text);
    void onClickYes();
};

#endif // WIDGET_H
