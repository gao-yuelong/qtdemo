#ifndef CUSTOMLISTITEMWIDGET_H
#define CUSTOMLISTITEMWIDGET_H

#include <QWidget>

namespace Ui {
class CustomListItemWidget;
}

class CustomListItemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CustomListItemWidget(QWidget *parent = nullptr);
    ~CustomListItemWidget();

private:
    Ui::CustomListItemWidget *ui;
};

#endif // CUSTOMLISTITEMWIDGET_H
