#include "customlistitemwidget.h"
#include "ui_customlistitemwidget.h"

CustomListItemWidget::CustomListItemWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CustomListItemWidget)
{
    ui->setupUi(this);
}

CustomListItemWidget::~CustomListItemWidget()
{
    delete ui;
}
