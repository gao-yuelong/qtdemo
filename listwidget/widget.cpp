﻿#include "widget.h"
#include "ui_widget.h"
#include "custommodel.h"
#include "customlistitemwidget.h"

#include <QListWidgetItem>
#include <QHBoxLayout>
#include <QLabel>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    // 1
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/new/prefix1/hat.png"), "123"));

    // 2
    addListWidgetItem("123");

    // 3
    custommodel *model = new custommodel(ui->listView);
    ui->listView->setModel(model);

    // 4
    QListWidgetItem *pItem = new QListWidgetItem(ui->listWidget, 0);
    pItem->setSizeHint(QSize(300, 60));
    QSize size = pItem->sizeHint();

    CustomListItemWidget *pCustomListItemWidget = new CustomListItemWidget(ui->listWidget);
    pCustomListItemWidget->setFixedSize(size);
    ui->listWidget->setItemWidget(pItem, pCustomListItemWidget);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::addListWidgetItem(QString strText)
{
    QListWidgetItem *pItem = new QListWidgetItem(ui->listWidget, 0);
    pItem->setSizeHint(QSize(ui->listWidget->width(), 60));
    QSize size = pItem->sizeHint();

    QWidget *pWidget = new QWidget(ui->listWidget);
    pWidget->setFixedSize(size);

    QLabel *pLabel1 = new QLabel(pWidget);
    pLabel1->setPixmap(QPixmap(":/new/prefix1/hat.png"));
    pLabel1->setScaledContents(true);

    QLabel *pLabel2 = new QLabel(pWidget);
    pLabel2->setText(strText);

    QLabel *pLabel3 = new QLabel(pWidget);
    pLabel3->setText(strText);

    QHBoxLayout *Hboxlayout = new QHBoxLayout(pWidget);
    Hboxlayout->addWidget(pLabel1);
    Hboxlayout->addWidget(pLabel2);
    Hboxlayout->addWidget(pLabel3);

    ui->listWidget->setItemWidget(pItem, pWidget);
}
