﻿#include "custommodel.h"
#include <QModelIndex>

custommodel::custommodel(QObject *parent)
    :QAbstractItemModel(parent)
{

}

int custommodel::columnCount(const QModelIndex &parent) const
{
    return 1;
}

QVariant custommodel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::DisplayRole)
    {
        return QVariant(QString::fromLocal8Bit("关注微信公众号：高二的笔记"));
    }

    return QVariant();
}

QModelIndex custommodel::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row, column);
}

QModelIndex custommodel::parent(const QModelIndex &index) const
{
    return QModelIndex();
}

int custommodel::rowCount(const QModelIndex &parent) const
{
    return 3;
}

//Qt::ItemFlags custommodel::flags(const QModelIndex &index) const
//{
//    return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
//}

//This view does not display horizontal or vertical headers; to display a list of items with a horizontal header, use QTreeView instead.
QVariant custommodel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
    {
     return QVariant("BING");
    }

    return QVariant();
}
