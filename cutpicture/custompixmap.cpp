﻿#include "custompixmap.h"
#include "common.h"
#include <QRectF>
#include <QGraphicsScene>
#include <QDebug>

custompixmap::custompixmap(const QPixmap &pixmap, QGraphicsItem *parent)
    :QGraphicsPixmapItem (pixmap, parent)
{

}

QRectF custompixmap::boundingRect() const
{
    return QGraphicsPixmapItem::boundingRect();
}

void custompixmap::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsPixmapItem::mouseMoveEvent(event);
}

QVariant custompixmap::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemPositionChange  &&  scene()) // 控件发生移动
    {
        QPointF newPos = value.toPointF(); //即将移动的位置
        //限制的区域
        QRectF rect(-scene()->width() / 3, -scene()->width() / 3, (scene()->width() / 3) * 2, (scene()->width() / 3) * 2);
        SHOW(rect)
        if (!rect.contains(newPos)) // 是否在区域内
        {
            newPos.setX(qMin(rect.right(), qMax(newPos.x(), rect.left())));
            newPos.setY(qMin(rect.bottom(), qMax(newPos.y(), rect.top())));
            return newPos;
        }
    }
    return QGraphicsItem::itemChange(change, value);
}
