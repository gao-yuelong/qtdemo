﻿#ifndef CUSTOMVIEW_H
#define CUSTOMVIEW_H

#include <QGraphicsView>
#include <QPoint>
class CustomView : public QGraphicsView
{
public:
    CustomView(QGraphicsScene *scene, QWidget *parent = nullptr);

    void paintEvent(QPaintEvent *);
    void wheelEvent(QWheelEvent *event);

    void grabPicture();

private:
    QPoint mPoint;//剪切的圆形的矩形的左上角点
};

#endif // CUSTOMVIEW_H
