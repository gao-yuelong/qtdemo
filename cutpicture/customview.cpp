﻿#include "customview.h"
#include "common.h"
#include <QWheelEvent>
#include <QPixmap>
#include <QPainter>

CustomView::CustomView(QGraphicsScene *scene, QWidget *parent)
    : QGraphicsView (scene, parent)
{

}

int radius = 150;//半径

void CustomView::paintEvent(QPaintEvent *e)
{
    //https://www.it610.com/article/3860174.htm
    //先调用QGraphicsView::paintEvent(e);再画自己的
    QGraphicsView::paintEvent(e);

    //https://www.codenong.com/cs106784396/
    QPainter painter(this->viewport());

    QPainterPath path1;
    //构建圆形路径
    mPoint.setX((width() / 2) - radius / 2);
    mPoint.setY((height() / 2) - radius / 2);
    path1.addEllipse(mPoint.x(), mPoint.y(), radius, radius);

    SHOW(radius)
    //设置颜色为半透明
    QColor color(192, 192, 192, 150);
    //填充除去圆形的范围
    painter.setBrush(QBrush(color));

    QPainterPath path;
    //整个view的路径
    path.addRect(rect());

    //path减去圆形的范围
    path -= path1;
    painter.setClipPath(path);

    //画矩形，注意，这里把圆形范围剔除了
    painter.drawRect(rect());
}

void CustomView::wheelEvent(QWheelEvent *event)
{
    QGraphicsView::wheelEvent(event);
    //限制圆形半径范围为[150,180]
    if(event->delta() > 0 && (radius - 10) >= 150){// 当滚轮远离使用者时
        radius -= 10;
    }else if (event->delta() < 0 && (radius + 10) <= 180){// 当滚轮向使用者方向旋转时
        radius += 10;
    }
    //https://www.codenong.com/cs106784396/
    this->viewport()->update();
}

//网上的方法，扣一个圆形图片出来
QPixmap PixmapToRound(const QPixmap &src, int radius)
{
    if (src.isNull()) {
        return QPixmap();
    }

    //构建一个透明的图片
    QPixmap pixmap(radius,radius);
    pixmap.fill(Qt::transparent);

    //在透明的图片上画
    QPainter painter(&pixmap);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    QPainterPath path;
    //画的范围为一个圆形
    path.addEllipse(0, 0, radius, radius);
    painter.setClipPath(path);
    //画图片
    painter.drawPixmap(0, 0, radius, radius, src);

    //返回这个图片
    return pixmap;
}
void CustomView::grabPicture()
{
    SHOW(mPoint)
    //按照圆形所在矩形范围，截取当前截图
    QPixmap pixmap = grab(QRect(mPoint.x(), mPoint.y(), radius, radius));

    //截完后，扣圆形图片
    pixmap = PixmapToRound(pixmap, radius);
    //保存图片
    pixmap.save("here.png");
}
