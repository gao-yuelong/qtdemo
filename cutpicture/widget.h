﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "customview.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_btnCut_clicked();

private:
    Ui::Widget *ui;
    CustomView *pCustomView;
};

#endif // WIDGET_H
