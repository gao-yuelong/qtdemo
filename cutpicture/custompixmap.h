﻿#ifndef CUSTOMPIXMAP_H
#define CUSTOMPIXMAP_H

#include <QGraphicsPixmapItem>

class custompixmap : public QGraphicsPixmapItem
{
public:
    custompixmap(const QPixmap &pixmap, QGraphicsItem *parent = nullptr);

    QRectF boundingRect() const override;

    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

};

#endif // CUSTOMPIXMAP_H

