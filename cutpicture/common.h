﻿#ifndef COMMON_H
#define COMMON_H

#ifdef DEBUG
    #include <QDebug>
#endif

#ifdef DEBUG
#define SHOW(x) qDebug() << #x << " : " << (x);
#else
#define SHOW(x)
#endif

#endif // COMMON_H
