﻿#include "widget.h"
#include "ui_widget.h"

#include <QGraphicsScene>
#include "custompixmap.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    QGraphicsScene *pScene = new QGraphicsScene();
    pScene->setSceneRect(0, 0, 220, 300);

    //QPixmap pixmap(":/new/prefix1/me.jpg");
    QPixmap pixmap(":/new/prefix1/girl.png");
    pixmap = pixmap.scaled(220, 300, Qt::KeepAspectRatio);

    custompixmap *pGraphicsPixmap = new custompixmap(pixmap);
    pGraphicsPixmap->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsGeometryChanges);

    pScene->addItem(pGraphicsPixmap);

    pCustomView = new CustomView(pScene, this);
    pCustomView->setGeometry(20, 20, 220, 300);
    pCustomView->show();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnCut_clicked()
{
    pCustomView->grabPicture();
}
