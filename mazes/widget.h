#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QVector>
#include <QPainter>
#include <QKeyEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget() override;
    void initUi();
    void initData();

    int find(int);
    void merge(int, int);
    int getNeighbor(int);
    void myDrawLine(int n, int m);

protected:
    void paintEvent(QPaintEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;

private slots:
    void processOneThing();

    void on_btnReset_clicked();

private:
    Ui::Widget *ui;

    QTimer *timer;

    QPixmap *m_pPixmap;
    QPainter *m_pPainter;

    QVector<int> m_qvectorArray;                //一维数组，即将图中的点抽象成一维数组的点
    QVector<int> m_qvectorSize;                 //用来记录并查集中每个集合点的数量
    QVector<QVector<int>> m_qvectorIsUnicom;    //用来记录两方块之间是否有线，也就是是否有障碍

    int m_iSquarePos;                           //记录方块的位置
    int m_iMs;
    int m_iSeconds;
    bool m_bIsGameRun;                          //是否运行游戏
    bool m_bIsSuccess;                          //游戏是否成功
};
#endif // WIDGET_H
