#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QTimer>
#include <QMessageBox>

#define BEGINX 30
#define ENDX 330
#define LINENUM (ENDX / BEGINX)
#define ROWNUM (LINENUM - 1)
#define SQUARESNUM (ROWNUM * ROWNUM)
#define LINEHEIGHT 30
#define LINEWIDTH 30

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , m_iSquarePos(0)
    , m_iMs(0)
    , m_iSeconds(0)
    , m_bIsGameRun(false)
    , m_bIsSuccess(true)
{
    ui->setupUi(this);

    setFocus();
    ui->btnReset->setFocusPolicy(Qt::NoFocus);

    m_pPixmap = new QPixmap(QWidget::size());
    m_pPainter = new QPainter;
    timer = new QTimer(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(processOneThing()));
    //初始化地图
    initUi();

    ui->lbPicture->setStyleSheet("border-image: url(:/new/prefix1/my.jpg);");
}

Widget::~Widget()
{
    delete ui;
}

//初始化地图，用黑色的线画出整个地图
void Widget::initUi()
{
    m_pPixmap->fill(Qt::white);
    m_pPainter->begin(m_pPixmap);

    QPen pen;
    pen.setColor(QColor(Qt::black));
    pen.setWidth(2);
    m_pPainter->setPen(pen);

    QLineF line;
    int x1 = BEGINX, y1 = 0, x2 = ENDX, y2 = 0;

    for (int i = 0; i < LINENUM; i++) {
        y1 += LINEHEIGHT;
        y2 = y1;

        line.setLine(x1, y1, x2, y2);
        m_pPainter->drawLine(line);

        line.setLine(y1, x1, y2, x2);
        m_pPainter->drawLine(line);
    }

    m_pPainter->end();
}

void Widget::initData()
{
    if (!m_qvectorIsUnicom.empty()) {
        m_qvectorIsUnicom.clear();
    }
    if (!m_qvectorSize.empty()) {
        m_qvectorSize.clear();
    }
    if (!m_qvectorArray.empty()) {
        m_qvectorArray.clear();
    }

    m_qvectorIsUnicom.resize(SQUARESNUM);
    m_qvectorSize.resize(SQUARESNUM);
    m_qvectorArray.resize(SQUARESNUM);

    //让起点和终点到达初始位置
    ui->btnGreen->move(BEGINX + 2, BEGINX + 2);
    ui->btnRed->move(ROWNUM * BEGINX + 2, ROWNUM * BEGINX + 2);

    for (int i = 0; i < m_qvectorArray.size(); i++) {
        m_qvectorArray[i] = i;
        m_qvectorIsUnicom[i].resize(SQUARESNUM);
        m_qvectorIsUnicom[i].fill(-1);
    }
    m_qvectorSize.fill(1);

    //如果起点和终点不属于一个集合，说明没有路能从起点走到终点，游戏自然就不成立。
    //因此，这里循环的目的就是让起点和终点属于一个集合，不断的找点合并，让游戏有解。
    while (find(0) != find(SQUARESNUM - 1)) {
        int randnum = rand() % SQUARESNUM;
        int neighbor = getNeighbor(randnum);
        if (find(randnum) != find(neighbor)) {
            m_qvectorIsUnicom[randnum][neighbor] = m_qvectorIsUnicom[neighbor][randnum] = 1;
            myDrawLine(randnum, neighbor);
            //merge属于并查集中的并
            merge(randnum, neighbor);
        }
    }
}

//找该集合的头儿（这里涉及并查集相关的概念）这是并查集中的查
int Widget::find(int n)
{
    if (m_qvectorArray[n] == n) return n;
    return find(m_qvectorArray[n]);
}

//将n和m合成一个集合
void Widget::merge(int n, int m)
{
    int fn = find(n), fm = find(m);
    if (fn == fm) return;
    if (m_qvectorSize[fn] > m_qvectorSize[fm]) std::swap(fn, fm);
    m_qvectorArray[fn] = fm;
    m_qvectorSize[fm] += m_qvectorSize[fn];
    return;
}

//画线，实则是用白色的画笔掩盖掉黑色的线，从而达到某些线消失不见的效果
void Widget::myDrawLine(int n, int m)
{
    int x1 = n / ROWNUM;
    int y1 = n % ROWNUM;
    int x2 = m / ROWNUM;
    int y2 = m % ROWNUM;

    int x3 = (x1 + x2) / 2 + 1;
    int y3 = (y1 + y2) / 2 + 1;

    QPen pen;
    pen.setColor(QColor(Qt::white));
    pen.setWidth(3);
    QLineF line;
    m_pPainter->begin(m_pPixmap);
    m_pPainter->setPen(pen);
    if (x1 - x2 == 1 || x1 - x2 == -1) {
        line.setLine(y1 * LINEWIDTH + BEGINX, x3 * LINEHEIGHT + BEGINX, (y1 + 1) * LINEWIDTH + BEGINX, (x3 + 1) * LINEHEIGHT);
    } else {
        line.setLine(y3 * LINEWIDTH + BEGINX, x1 * LINEHEIGHT + BEGINX, (y3 + 1) * LINEWIDTH, (x1 + 1) * LINEHEIGHT + BEGINX);
    }
    m_pPainter->drawLine(line);
    m_pPainter->end();
}

//取n的上下左右方向随机的一个邻居
int Widget::getNeighbor(int n)
{
    int x = n / ROWNUM;
    int y = n % ROWNUM;
    QVector<int> arr;
    if (y > 0) arr.push_back(n - 1);
    if (y < ROWNUM - 1) arr.push_back(n + 1);
    if (x > 0) arr.push_back((x - 1) * ROWNUM + y);
    if (x < ROWNUM - 1) arr.push_back((x + 1) * ROWNUM + y);
    int tmp = rand() % arr.size();
    return arr[tmp];
}

//窗体绘制事件
void Widget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawPixmap(QPoint(0, 0), *m_pPixmap);
    //如果当前时间小于60，并且绿色的点坐标和红色的点坐标相同，说明走出迷宫，游戏成功
    if (m_iSeconds < 60 && ui->btnGreen->pos() == ui->btnRed->pos()) {
        ui->lbMsg->setStyleSheet("color:green");
        ui->lbMsg->setText("successful");
        timer->stop();
        m_bIsGameRun = false;
        m_bIsSuccess = true;
    }
    if (m_iSeconds >= 60 && ui->btnGreen->pos() != ui->btnRed->pos()) {
        ui->lbMsg->setStyleSheet("color:red");
        ui->lbMsg->setText("failure");
        timer->stop();
        m_bIsGameRun = false;
        m_bIsSuccess = false;
    }
}
//按键事件
void Widget::keyPressEvent(QKeyEvent *event)
{
    if (!m_bIsSuccess) return;
    if (!m_bIsGameRun) {
        timer->start();
        m_bIsGameRun = true;
    }

    QPoint point = ui->btnGreen->pos();
    int tmp = m_iSquarePos;
    if (event->key() == Qt::Key_Left) {
        if (point.x() - LINEWIDTH > BEGINX) {
            point.setX(point.x() - LINEWIDTH);
            m_iSquarePos -= 1;
        }
    } else if (event->key() == Qt::Key_Right) {
        if (point.x() + LINEWIDTH < ENDX) {
            point.setX(point.x() + LINEWIDTH);
            m_iSquarePos += 1;
        }
    } else if (event->key() == Qt::Key_Up) {
        if (point.y() - LINEHEIGHT > BEGINX) {
            m_iSquarePos -= ROWNUM;
            point.setY(point.y() - LINEHEIGHT);
        }
    } else if (event->key() == Qt::Key_Down) {
        if (point.y() + LINEHEIGHT < ENDX) {
            m_iSquarePos += ROWNUM;
            point.setY(point.y() + LINEHEIGHT);
        }
    }
    if (m_qvectorIsUnicom[tmp][m_iSquarePos] != -1) {
        ui->btnGreen->move(point);
    } else {
        m_iSquarePos = tmp;
    }

}

//定时器定时触发该函数，从而达到计数的效果
void Widget::processOneThing()
{
    ui->lbMs->setText(QString("%1").arg(++m_iMs));
    if (m_iMs == 1000) {
        m_iMs = 0;
        ui->lbSec->setText(QString("%1").arg(++m_iSeconds));
    }
    update();
}

//重置按钮按下，初始化地图和变量
void Widget::on_btnReset_clicked()
{
    QPixmap *clearPix = new QPixmap(size());
    clearPix->fill (Qt::white);
    m_pPixmap = clearPix;
    update();

    timer->stop();
    m_iSquarePos = 0;
    m_iSeconds = 0;
    m_iMs = 0;
    ui->lbMs->setText("0");
    ui->lbSec->setText("0");
    ui->lbMsg->setText("");

    m_bIsGameRun = false;
    m_bIsSuccess = true;

    initUi();
    initData();
}
