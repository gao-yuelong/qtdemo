#ifndef PRIZEWINDOW_H
#define PRIZEWINDOW_H

#include <QWidget>
#include <QSet>

namespace Ui {
class PrizeWindow;
}

class PrizeWindow : public QWidget
{
    Q_OBJECT

public:
    explicit PrizeWindow(QWidget *parent = nullptr);
    ~PrizeWindow() Q_DECL_OVERRIDE;
    void init();
    void resizeEvent(QResizeEvent *) Q_DECL_OVERRIDE;

private:
    Ui::PrizeWindow *ui;
    QSet<QWidget*> m_setWidget;
};

#endif // PRIZEWINDOW_H
