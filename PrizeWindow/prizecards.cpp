#include "prizecards.h"
#include "ui_prizecards.h"
#include <QFile>

PrizeCards::PrizeCards(QWidget *parent, const QString &strFilePath) :
    QWidget(parent),
    ui(new Ui::PrizeCards),
    m_strFilePath(strFilePath)
{
    ui->setupUi(this);
    setStyleSheet("border-radius: 4px;border: 1px solid red;");
    QFile file(m_strFilePath);
    if (file.exists()) {
        ui->label->setPixmap(m_strFilePath);
    }
}

PrizeCards::~PrizeCards()
{
    delete ui;
}
