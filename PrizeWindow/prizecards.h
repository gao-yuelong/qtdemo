#ifndef PRIZECARDS_H
#define PRIZECARDS_H

#include <QWidget>

namespace Ui {
class PrizeCards;
}

class PrizeCards : public QWidget
{
    Q_OBJECT

public:
    PrizeCards(QWidget *parent, const QString &strFilePath);
    ~PrizeCards();

private:
    Ui::PrizeCards *ui;
    QString m_strFilePath;
};

#endif // PRIZECARDS_H
