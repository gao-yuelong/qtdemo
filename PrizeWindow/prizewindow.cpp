#include "prizewindow.h"
#include "ui_prizewindow.h"
#include "prizecards.h"

#include <QResizeEvent>
#include <QDir>
#include <QDebug>
#include <QScrollBar>

#define COLNUM 5
#define ROWNUM 3

PrizeWindow::PrizeWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PrizeWindow)
{
    ui->setupUi(this);
    init();
}

PrizeWindow::~PrizeWindow()
{
    delete ui;
}

void PrizeWindow::init()
{
    QDir dir(":/images/");
    QStringList strlistFilePath = dir.entryList();

    const int iVacancyNum = 0;
    for (int i = iVacancyNum; i < strlistFilePath.size() + iVacancyNum; i++) {
        QString strFilePath = ":/images/" + strlistFilePath[i - iVacancyNum];
        PrizeCards *pPrizeCards = new PrizeCards(this, strFilePath);

        ui->gridLayout->addWidget(pPrizeCards, i / COLNUM, i % COLNUM);

        m_setWidget.insert(pPrizeCards);
    }
    //多增加一页，为了测试带滚动条的效果
//    PrizeCards *pPrizeCards = new PrizeCards(this, ":/images/bixin.png");
//    ui->gridLayout->addWidget(pPrizeCards, 3, 0);
//    m_setWidget.insert(pPrizeCards);
}

void PrizeWindow::resizeEvent(QResizeEvent *pEvent)
{
    //根据当前窗口大小计算每个卡片的大小
    int width = ui->scrollArea->size().width() / COLNUM - 7;
    int height = ui->scrollArea->size().height() / ROWNUM - 13;
    //遍历set，设置卡片大小
    for (auto s : m_setWidget) {
        s->setFixedSize(width, height);
    }
    QWidget::resizeEvent(pEvent);
}
