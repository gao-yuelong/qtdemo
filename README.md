# QtDemo

环境：Qt 5.12 MSVC 2015 64bit

demo1 不规则图形登录页面

demo2 目录及多文档窗口

demo3 基于某一个模板窗体派生出多样化的子类窗体

demo4 鼠标悬浮在图片上时，出现删除字样，点击删除可以删除该图片

MorningPaper 写日报小程序，每天在上班前可以回忆昨天工作内容，写下今天工作计划

mazes 迷宫小游戏（并查集算法实现）

ChristmasTree圣诞树

Lucky抽奖小游戏

Paopao一个智能聊天机器人

PrizeWindow卡片橱窗（主要是QGridLayout的应用，比如如何添加卡片并按照从左到右，从上到下的顺序排列）

graphsimple 可拖拉的小图片(QGraphicsScene的应用)

ChangeShapeDialog 可改变形状的Dialog（来自书籍《C++ GUI Qt4编程》第二版2.4章节例子）

cutpicture截取圆形头像小工具

internationalization国际化翻译解决方案

listmodelapp 一个关于listModel的应用，可以了解自定义model的使用