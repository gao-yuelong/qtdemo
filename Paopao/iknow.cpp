#include "iknow.h"

#include <QProcess>
#include <QDebug>
#include <QTextCodec>
#include <QCoreApplication>

IKnow::IKnow()
{
    QString strPorc = QCoreApplication::applicationDirPath() + "/IKNOW.exe";

    m_pProc = new QProcess();
    m_pProc->setReadChannel(QProcess::StandardOutput);
    m_pProc->start(strPorc);

    connect(m_pProc, SIGNAL(readyReadStandardOutput()), this, SLOT(recvStandOut()));
}

IKnow::~IKnow()
{
    m_pProc->kill();
    delete m_pProc;
    m_pProc = nullptr;
}

void IKnow::recvStandOut()
{
    exec();
}

QString IKnow::answer()
{
    return m_strQuestion;
}

void IKnow::setQuestion(QString strQuestion)
{
    //向IKNOW.exe发送消息
    QByteArray byte = strQuestion.toLocal8Bit();
    const char *pChar = byte.data();
    m_pProc->write(pChar);
    m_pProc->write("\r\n");
    qDebug() << m_pProc->waitForBytesWritten(2000);
}

bool IKnow::findAnswer()
{
    //收到IKNOW.exe的消息
    QByteArray byte = m_pProc->readAllStandardOutput();
    QTextCodec *pTextCodec = QTextCodec::codecForName("GBK");
    QString strMsg = pTextCodec->toUnicode(byte);
    if (strMsg.isEmpty()) {
        return false;
    }
    //用换车替换{br}
    strMsg.replace("{br}", QChar('\n'));

    m_strQuestion = strMsg;
    return true;
}

void IKnow::exec()
{
    if (findAnswer()) {
        emit signalFindAnswer();
    }
}
