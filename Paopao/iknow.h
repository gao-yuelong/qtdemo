#ifndef IKNOW_H
#define IKNOW_H

#include <QString>
#include <QObject>

class QProcess;
class IKnow : public QObject
{
    Q_OBJECT
public:
    IKnow();
    ~IKnow();
    QString answer();
    void setQuestion(QString);
    bool findAnswer();
    void exec();

signals:
    void signalFindAnswer();

private slots:
    void recvStandOut();

private:
    QProcess *m_pProc;
    QString m_strAnswer;
    QString m_strQuestion;
};

#endif // IKNOW_H
