#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHBoxLayout>
#include <QTreeWidget>
#include <QMdiArea>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void initTreeWidget();
    void initWelcome();

private:

    QHBoxLayout *m_phboxLayout;
    QTreeWidget *m_ptreewgtWgt;
    QMdiArea *m_pmdiareaArea;

private slots:
    void emititemClicked(QTreeWidgetItem *, int);

};

#endif // MAINWINDOW_H
