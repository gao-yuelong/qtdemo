#include "mainwindow.h"
#include <QTextEdit>
#include <QDebug>
#include <QMdiSubWindow>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    m_phboxLayout = new QHBoxLayout();
    m_ptreewgtWgt = new QTreeWidget();
    m_ptreewgtWgt->setHeaderLabel(tr("目录"));

    m_pmdiareaArea = new QMdiArea();
    m_pmdiareaArea->setViewMode(QMdiArea::TabbedView);
    m_pmdiareaArea->setTabsClosable(true);

    initTreeWidget();
    initWelcome();

    QWidget *centerWindow = new QWidget;
    this->setCentralWidget(centerWindow);

    m_phboxLayout->addWidget(m_ptreewgtWgt, 1);
    m_phboxLayout->addWidget(m_pmdiareaArea, 5);

    centerWindow->setLayout(m_phboxLayout);
    connect(m_ptreewgtWgt,SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this,SLOT(emititemClicked(QTreeWidgetItem* ,int)));
}

void MainWindow::initTreeWidget()
{
    QTreeWidgetItem *qtreewgtTitle1 = new QTreeWidgetItem(m_ptreewgtWgt);
    qtreewgtTitle1->setText(0, "Title1");
    QTreeWidgetItem *qtreewgtcapt1 = new QTreeWidgetItem(qtreewgtTitle1);
    qtreewgtcapt1->setText(0, "capter1");

    QTreeWidgetItem *qtreewgtTitle2 = new QTreeWidgetItem(m_ptreewgtWgt);
    qtreewgtTitle2->setText(0, "Title2");
    QTreeWidgetItem *qtreewgtcapt2 = new QTreeWidgetItem(qtreewgtTitle2);
    qtreewgtcapt2->setText(0, "capter2");

}

void MainWindow::initWelcome()
{
    QTextEdit *qtexteditWel = new QTextEdit();
    qtexteditWel->setHtml("<div><div style=\"text-align:center;\"><b>One small example of Qt that doesn't work every week</b></div><div style=\"text-align:center; width:100%; position:fixed;\"><img src=':/new/res/me.jpg' height=\"150\" width=\"150\"></div> </div>");
    qtexteditWel->setWindowTitle("Welcome");
    qtexteditWel->setReadOnly(true);
    m_pmdiareaArea->addSubWindow(qtexteditWel);
    qtexteditWel->showMaximized();
}


void MainWindow::emititemClicked(QTreeWidgetItem *item, int column)
{
    QTextEdit *qtexteditPage = new QTextEdit();
    qtexteditPage->setWindowTitle(item->text(column));
    QString strContents("<b>this is :" + item->text(column) + "</b>");
    qtexteditPage->setHtml(strContents);

    m_pmdiareaArea->addSubWindow(qtexteditPage);
    qtexteditPage->showMaximized();

}

MainWindow::~MainWindow()
{

}
