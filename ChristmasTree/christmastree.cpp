#include "christmastree.h"
#include "ui_christmastree.h"

#include <QMouseEvent>
#include <QDebug>
#include <QMenu>
#include <QPainter>
#include <QRandomGenerator>
#include <QLabel>
#include <QRectF>

christmasTree::christmasTree(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::christmasTree)
{
    ui->setupUi(this);
    //窗体无边框和窗体置顶
    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    //背景透明
    setAttribute(Qt::WA_TranslucentBackground);

    //记录窗体的宽和高
    m_iWidth = width();
    m_iHeight = height();

    m_pTimer1 = new QTimer();
    connect(m_pTimer1, SIGNAL(timeout()), this, SLOT(refreshFast()));
    m_pTimer1->start(50);

    m_pTimer2 = new QTimer();
    connect(m_pTimer2, SIGNAL(timeout()), this, SLOT(refreshSlow()));
    m_pTimer2->start(1000);

    //树干三条横线占窗体高的比例
    m_vecTrunkNode = {0.9, 0.7, 0.5};

    //生成左右的树枝线
    generateTreeBranch();

    //生成树干上的三条横线
    generateTrunk();

    //树顶的星星
    ui->lbSdxx->setGeometry(m_iWidth / 2 - 15, m_iHeight * 0.3 - 12, 30, 30);
    ui->lbSdxx->setStyleSheet("border-image: url(:/new/prefix1/sdxx.png);");
    //红包
    ui->lbHB1->setStyleSheet("border-image: url(:/new/prefix1/hongbao.png);");
    ui->lbHB1->setVisible(false);

    //把所有的星星放入vec中
    m_vecStars.push_back(ui->lbStar1);
    m_vecStars.push_back(ui->lbStar2);
    m_vecStars.push_back(ui->lbStar3);
}

christmasTree::~christmasTree()
{
    delete m_pTimer1;
    delete m_pTimer2;
    delete ui;
}

void christmasTree::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        QMenu menu;
        menu.addAction("close", this, SLOT(close()));
        menu.exec(cursor().pos());
    }
    m_pos = event->globalPos() - geometry().topLeft();
}

void christmasTree::mouseMoveEvent(QMouseEvent *event)
{
    move(event->globalPos() - m_pos);
}

void christmasTree::mouseReleaseEvent(QMouseEvent *)
{
    m_pos = QPoint();
}

void christmasTree::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.setFont(QFont("Arial", 25));
    const QRect rectangle = QRect(0, 20, m_iWidth, 50);
    //写字
    painter.drawText(rectangle, "Merry Christmas");

    QPen pen;
    pen.setColor(QColor(140, 90, 50));//树根的颜色
    pen.setWidth(10);
    pen.setJoinStyle(Qt::RoundJoin);
    pen.setCapStyle(Qt::RoundCap);

    painter.setPen(pen);

    //画树干（竖直的线）
    QLineF lineTreeTrunk(m_iWidth / 2, m_iHeight, m_iWidth / 2, m_iHeight * 0.3);
    painter.drawLine(lineTreeTrunk);

    //设置树枝的颜色，准备画树枝
    pen.setColor(QColor(0, 100, 0));
    painter.setPen(pen);

    painter.drawLines(m_vecTrunk);
    painter.drawLines(m_vecTreeBranch);

    //窗体背景
    painter.fillRect(rect(), QColor(0, 0, 0, 1));

    //画灯（这里可以优化做一个呼吸灯）
    QRectF rectLed1(m_iWidth / 3, m_iHeight - 50, 20.0, 20.0);
    QRectF rectLed2(m_iWidth * 0.6, m_iHeight - 150, 20.0, 20.0);

    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(255, 255, 0));

    painter.drawEllipse(rectLed1);
    painter.drawEllipse(rectLed2);

}

//生成三条横线
void christmasTree::generateTrunk()
{
    m_vecTrunk.clear();
    //画三层（树干的三条横线）
    for (int i = 3; i >= 1; i--) {
        QLineF line(m_iWidth * 0.1 * i, m_iHeight * m_vecTrunkNode[i - 1], m_iWidth - m_iWidth * 0.1 * i, m_iHeight * m_vecTrunkNode[i - 1]);
        m_vecTrunk.push_back(line);
    }
}

//生成左右两边的线
void christmasTree::generateTreeBranch()
{
    for (int i = 3; i >= 1; i--) {
        QLineF lineLeft(m_iWidth * (0.1 * i + 0.2), m_iHeight * (m_vecTrunkNode[i - 1] - 0.2), m_iWidth * 0.1 * i, m_iHeight * m_vecTrunkNode[i - 1]);
        QLineF lineRight(m_iWidth - m_iWidth * (0.1 * i + 0.2), m_iHeight * (m_vecTrunkNode[i - 1] - 0.2), m_iWidth - m_iWidth * 0.1 * i, m_iHeight * m_vecTrunkNode[i - 1]);
        m_vecTreeBranch.push_back(lineLeft);
        m_vecTreeBranch.push_back(lineRight);
    }
}

void christmasTree::refreshFast()
{
    refreshRedPacket();
}

void christmasTree::refreshSlow()
{
    refreshStars();
}

void christmasTree::refreshRedPacket()
{
    //如果红包1不可见
    if (!ui->lbHB1->isVisible()) {
        //产生一个随机位置，位于上部分
        int iX = QRandomGenerator::global()->bounded(m_iWidth - 10) + 5;
        ui->lbHB1->setGeometry(iX, 10, 30, 30);
        ui->lbHB1->setVisible(true);
    }
    if (ui->lbHB1->pos().y() + 1 > m_iHeight) {
        ui->lbHB1->setVisible(false);
        return;
    }
    //红包下移的效果
    ui->lbHB1->setGeometry(ui->lbHB1->pos().x(), ui->lbHB1->pos().y() + 1, 30, 30);
}

void christmasTree::refreshStars()
{
    //存放不可见的星星
    QVector<QLabel *> vecNotVisibleStars;
    //存放可见的星星
    QVector<QLabel *> vecVisibleStars;

    //遍历所有的星星
    for (auto vec : m_vecStars) {
        if (vec->isVisible()) {
            vecVisibleStars.push_back(vec);
        } else {
            //不可见的星星
            vecNotVisibleStars.push_back(vec);
        }
    }
    //如果星星有一些不可见，则在不可见的星星中随机显示一个
    if (vecVisibleStars.size() != m_vecStars.size()) {
        int iVisibleStar = QRandomGenerator::global()->bounded(vecNotVisibleStars.size());
        int iPosX = QRandomGenerator::global()->bounded(10, m_iWidth - 10);
        int iPosY = QRandomGenerator::global()->bounded(10, m_iHeight - 10);
        vecNotVisibleStars[iVisibleStar]->setGeometry(iPosX, iPosY, 12, 12);
        vecNotVisibleStars[iVisibleStar]->setVisible(true);
    }
    //每次消失一个星星
    vecVisibleStars[QRandomGenerator::global()->bounded(vecVisibleStars.size())]->setVisible(false);
}
