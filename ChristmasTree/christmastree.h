#ifndef CHRISTMASTREE_H
#define CHRISTMASTREE_H

#include <QWidget>
#include <QPoint>
#include <QTimer>
#include <QLabel>

namespace Ui {
class christmasTree;
}

class christmasTree : public QWidget
{
    Q_OBJECT

public:
    explicit christmasTree(QWidget *parent = nullptr);
    ~christmasTree() override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;

    void generateTrunk();
    void generateTreeBranch();

    void refreshRedPacket();
    void refreshStars();

private slots:
    void refreshFast();
    void refreshSlow();

private:
    Ui::christmasTree *ui;
    QPoint m_pos;
    QVector<QLineF> m_vecTrunk;         //树干
    QVector<QLineF> m_vecTreeBranch;    //树枝
    int m_iWidth;                          //窗体宽度
    int m_iHeight;                         //窗体高度
    QVector<qreal> m_vecTrunkNode;
    QVector<qreal> m_vecBranchNode;

    QTimer *m_pTimer1;                    //定时器
    QTimer *m_pTimer2;                    //定时器

    QVector<QLabel *> m_vecStars;           //星星
};

#endif // CHRISTMASTREE_H
