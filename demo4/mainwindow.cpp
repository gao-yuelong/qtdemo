#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->centralWidget->setMouseTracking(true);
    setMouseTracking(true);
    ui->label->setMouseTracking(true);
    ui->label->hide();
    ui->pushButton_2->setMouseTracking(true);
    ui->pushButton_2->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QPixmap pixmap;
    QString fileName = QFileDialog::getOpenFileName(this,
          tr("Open Image"), "/", tr("Image Files (*.png *.jpg)"));
    if (!fileName.isEmpty()) {
        pixmap.load(fileName);
        ui->label->show();
        ui->label->setScaledContents(true);
        ui->label->setPixmap(pixmap);
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    QRect rect(ui->label->pos(), ui->label->size());
    ui->pushButton_2->setVisible(rect.contains(event->pos()) && ui->label->isVisible());
    qDebug() << ui->label->pos();
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->label->clear();
    ui->label->hide();
    ui->pushButton_2->hide();
}
