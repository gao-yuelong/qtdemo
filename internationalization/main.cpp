﻿#include "widget.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //国际化翻译
    QTranslator translator;
    translator.load("en_to_zh.qm", ":/new/prefix1/qm/");
    a.installTranslator(&translator);

    Widget w;
    w.show();

    return a.exec();
}
