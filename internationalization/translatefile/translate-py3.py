import csv
import xml.etree.ElementTree as ET
import os

dict = {}

def parseCsv(fileName):
    with open(fileName, 'r', encoding='utf8') as f:
        reader = csv.reader(f)
        for row in reader:
            strSText = row[0]
            strDText = row[1]
            dict[strSText] = strDText

def updateTSFile(tsFileName):
    dom = ET.parse(tsFileName)
    root = dom.getroot()
    for child in root:
        msgList = child.findall("message")
        for msg in msgList:
            strSText = ""
            for mmsg in msg:
                if mmsg.tag == 'source':
                    strSText = mmsg.text
                if mmsg.tag == 'translation':
                    #if find strSText
                    if strSText in dict:
                        mmsg.text = dict[strSText]
                        del mmsg.attrib['type']
    dom.write(tsFileName)
if __name__ == '__main__':
    #读csv文件
    parseCsv(os.getcwd() + "//translate.csv")
    #en_to_zh.ts
    updateTSFile(os.getcwd() + "/en_to_zh.ts")