Qt国际化翻译自动脚本
分为三部分：translate.bat、translate.csv、translate-py3.py

手动点击translate.bat开始执行脚本，脚本会生成翻译后的qm文件。
（执行前需要配置Qt安装路径及python安装路径）

translate.csv中存放源语言文本及目标语言文本，具体如下：
En,Zh
Set Start,设置为起点
Set End,设置为终点
比如将英文翻译为中文，按照上述格式写即可。

指定translate.bat时，会调用translate-py3.py做一些翻译工作。

脚本执行后会生成en_to_zh.ts和en_to_zh.qm两个文件，其中.qm文件是程序运行实际加载的翻译文件。