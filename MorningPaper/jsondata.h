#ifndef JSONDATA_H
#define JSONDATA_H

#include <QVector>
#include <QString>

class JsonData
{
public:
    JsonData();
    ~JsonData();
    int writeData(int iWeekNum, const QVector<QString> &vecData);
    QVector<QString> readData(int iWeekNum);
    QString logOfTheDay(int iWeekNum, int iDayNum);
};

#endif // JSONDATA_H
