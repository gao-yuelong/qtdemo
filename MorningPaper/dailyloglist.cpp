#include <QDialog>
#include <QDebug>
#include "dailyloglist.h"
#include "ui_dailyloglist.h"

DailyLogList::DailyLogList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DailyLogList),
    m_strYesterDayLog(""),
    m_strTodayLog("")
{
    ui->setupUi(this);
    ui->lineEdit->setAttribute(Qt::WA_Hover, true);
}

DailyLogList::~DailyLogList()
{
    delete ui;
}

//edit dailyloglist
void DailyLogList::on_btnEdit_clicked()
{
    DailyLogEditPanel pDailylogeditpanel;

    if (!m_strYesterDayLog.isEmpty() || !m_strTodayLog.isEmpty()) {
        pDailylogeditpanel.setYesterDayLog(m_strYesterDayLog);
        pDailylogeditpanel.setTodayLog(m_strTodayLog);
    }

    int iRet = pDailylogeditpanel.exec();
    if (iRet == QDialog::Accepted) {
        m_strYesterDayLog = pDailylogeditpanel.getYesterDayLog();
        m_strTodayLog =  pDailylogeditpanel.getTodayLog();
        updateData();
    } else if (iRet == QDialog::Rejected) {

    }
}

QString DailyLogList::getYesterDayLog()
{
    return m_strYesterDayLog;
}

QString DailyLogList::getTodayLog()
{
    return m_strTodayLog;
}

void DailyLogList::setYesterDayLog(QString strLog)
{
    m_strYesterDayLog = strLog;
    updateData();
}

void DailyLogList::setTodayLog(QString strLog)
{
    m_strTodayLog = strLog;
    updateData();
}


void DailyLogList::setDate(QString strDate)
{
    ui->lbDate->setText(strDate);
}

void DailyLogList::setDateColorRed()
{
    ui->lbDate->setStyleSheet("color:red");
}

void DailyLogList::updateData()
{
    if (m_strYesterDayLog == "" && m_strTodayLog == "") return;
    QString strContent = "yesterday: " + m_strYesterDayLog + "\n" +
            "today: " + m_strTodayLog;

    ui->lineEdit->setText(strContent);
    ui->lineEdit->setToolTip(strContent);
}
