#ifndef DAILYLOGLIST_H
#define DAILYLOGLIST_H

#include <QWidget>
#include "dailylogeditpanel.h"

namespace Ui {
class DailyLogList;
}

class DailyLogList : public QWidget
{
    Q_OBJECT

public:
    explicit DailyLogList(QWidget *parent = nullptr);
    ~DailyLogList();

    void setDate(QString);
    void setDateColorRed();

    QString getYesterDayLog();
    void setYesterDayLog(QString);

    QString getTodayLog();
    void setTodayLog(QString);

    void updateData();

private slots:
    void on_btnEdit_clicked();

private:
    Ui::DailyLogList *ui;
    QString m_strYesterDayLog;
    QString m_strTodayLog;
};

#endif // DAILYLOGLIST_H
