#ifndef MORNINGPAPER_H
#define MORNINGPAPER_H

#include <QDialog>
#include "dailyloglist.h"

#define DAILYNUM 7

namespace Ui {
class MorningPaper;
}

class MorningPaper : public QDialog
{
    Q_OBJECT

public:
    explicit MorningPaper(QWidget *parent = nullptr);
    ~MorningPaper();
    void initUi();
    void updateDate();

private slots:
    void on_btnsave_clicked();

private:
    Ui::MorningPaper *ui;
    DailyLogList *m_pDailyloglist[DAILYNUM];
};

#endif // MORNINGPAPER_H
