#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDebug>
#include "jsondata.h"

#define FILENAME "./morningpaper.json"

JsonData::JsonData()
{

}

JsonData::~JsonData()
{

}

int JsonData::writeData(int iWeekNum, const QVector<QString> &vecData)
{
    QFile file(FILENAME);
    if (!file.open(QIODevice::ReadWrite)) {
        qDebug() << "open file failure" << endl;
        return -1;
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());

    //清空文件
    file.resize(0);

    //判断之前是否存在
    QJsonObject jsonObj = jsonDoc.object();

    QString strWeekKey = "week" + QString::number(iWeekNum);

    QJsonObject jsonObjDay;
    for (int i = 0; i < vecData.size(); i++) {
        QString strDayKeys = "day" + QString::number(i);
        jsonObjDay[strDayKeys] = vecData[i];
    }

    if (jsonObj.isEmpty()) {
        QJsonObject jsonObjWeekNum;
        jsonObjWeekNum.insert(strWeekKey, jsonObjDay);
        jsonObj = jsonObjWeekNum;
        jsonDoc.setObject(jsonObj);
        file.write(jsonDoc.toJson());
        file.close();
        return 0;
    }


    if (jsonObj.contains(strWeekKey)) {
        jsonObj[strWeekKey] = jsonObjDay;
    } else {
        jsonObj.insert(strWeekKey, jsonObjDay);

    }

    jsonDoc.setObject(jsonObj);
    file.write(jsonDoc.toJson());
    file.close();
    return 0;
}

QVector<QString> JsonData::readData(int iWeekNum)
{
    QVector<QString> vec(7);
    QFile file(FILENAME);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "open file failure" << endl;
        vec.resize(0);
        return vec;
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());

    QJsonObject jsonObj = jsonDoc.object();

    QString strWeekKey = "week" + QString::number(iWeekNum);

    if (jsonObj.contains(strWeekKey)) {
        QJsonObject jsonObjectDay = jsonObj[strWeekKey].toObject();
        for (auto key : jsonObjectDay.keys()) {
            QString strDayNum = key.mid(3);
            vec[strDayNum.toInt()] = jsonObjectDay[key].toString();
        }
    }

    file.close();
    return vec;
}

QString JsonData::logOfTheDay(int iWeekNum, int iDayNum)
{
    QString strLog;
    QFile file(FILENAME);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "open file failure" << endl;
        return strLog;
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());

    QJsonObject jsonObj = jsonDoc.object();

    QString strWeekKey = "week" + QString::number(iWeekNum);

    if (jsonObj.contains(strWeekKey)) {
        QJsonObject jsonObjectDay = jsonObj[strWeekKey].toObject();
        QString strDayKey = "day" + QString::number(iDayNum);
        strLog = jsonObjectDay[strDayKey].toString();
    }

    file.close();
    return strLog;
}
