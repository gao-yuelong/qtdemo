#ifndef DAILYLOGEDITPANEL_H
#define DAILYLOGEDITPANEL_H

#include <QDialog>

namespace Ui {
class DailyLogEditPanel;
}

class DailyLogEditPanel : public QDialog
{
    Q_OBJECT

public:
    explicit DailyLogEditPanel(QDialog *parent = nullptr);
    ~DailyLogEditPanel();

    QString getYesterDayLog();
    void setYesterDayLog(QString);

    QString getTodayLog();
    void setTodayLog(QString);

private slots:
    void on_btnOk_clicked();
    void on_btnCancel_clicked();

private:
    Ui::DailyLogEditPanel *ui;
    QString m_strYesterDayLog;
    QString m_strTodayLog;
};

#endif // DAILYLOGEDITPANEL_H
