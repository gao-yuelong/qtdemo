#include "dailylogeditpanel.h"
#include "ui_dailylogeditpanel.h"

DailyLogEditPanel::DailyLogEditPanel(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::DailyLogEditPanel),
    m_strYesterDayLog(""),
    m_strTodayLog("")
{
    ui->setupUi(this);
    ui->editYesterDay->setEnabled(false);
}

DailyLogEditPanel::~DailyLogEditPanel()
{
    delete ui;
}

QString DailyLogEditPanel::getYesterDayLog()
{
    return m_strYesterDayLog;
}

QString DailyLogEditPanel::getTodayLog()
{
    return m_strTodayLog;
}

void DailyLogEditPanel::setYesterDayLog(QString strLog)
{
    ui->editYesterDay->setText(strLog);
}

void DailyLogEditPanel::setTodayLog(QString strLog)
{
    ui->editToday->setText(strLog);

    QTextCursor textcursor = ui->editToday->textCursor();
    textcursor.movePosition(QTextCursor::End);
    ui->editToday->setTextCursor(textcursor);
}

void DailyLogEditPanel::on_btnOk_clicked()
{
    m_strYesterDayLog = ui->editYesterDay->toPlainText();
    m_strTodayLog = ui->editToday->toPlainText();
    accept();
}

void DailyLogEditPanel::on_btnCancel_clicked()
{
    reject();
}
