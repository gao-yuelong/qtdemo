#include <QDateTime>
#include <QDebug>
#include <QMessageBox>
#include "jsondata.h"
#include "morningpaper.h"
#include "ui_morningpaper.h"

MorningPaper::MorningPaper(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MorningPaper)
{
    ui->setupUi(this);
    setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);
    ui->lbPicture->setStyleSheet("border-image: url(:/new/prefix1/my.jpg);");
    initUi();
}

MorningPaper::~MorningPaper()
{
    delete ui;
}

void MorningPaper::initUi()
{
    for (int i = 0; i < DAILYNUM; i++) {
        m_pDailyloglist[i] = new DailyLogList();
        ui->verticalLayout->insertWidget(i, m_pDailyloglist[i]);
    }
    updateDate();
}

void MorningPaper::updateDate()
{
    JsonData jsonData;
    QDateTime dateTime;
    int iDayofWeek = dateTime.currentDateTime().date().dayOfWeek() - 1;

    m_pDailyloglist[iDayofWeek]->setDateColorRed();

    //after
    for (int i = iDayofWeek; i < DAILYNUM; i++) {
        m_pDailyloglist[i]->setDate(dateTime.currentDateTime().date().addDays(i - iDayofWeek).toString());
    }

    //before
    for (int i = 0; i < iDayofWeek; i++) {
        m_pDailyloglist[i]->setDate(dateTime.currentDateTime().date().addDays((iDayofWeek - i) * -1).toString());
    }

    int iWeekNum = dateTime.currentDateTime().date().weekNumber();

    if (jsonData.readData(iWeekNum).isEmpty()) {
        return;
    }

    for (int i = 0; i <= iDayofWeek; i++) {
        if (i == 0) {
            if (!jsonData.readData(iWeekNum - 1).isEmpty()) {
                m_pDailyloglist[i]->setYesterDayLog(jsonData.logOfTheDay(iWeekNum - 1, DAILYNUM - 1));
            }
            m_pDailyloglist[i]->setTodayLog(jsonData.logOfTheDay(iWeekNum, i));
            continue;
        }
        m_pDailyloglist[i]->setYesterDayLog(jsonData.logOfTheDay(iWeekNum, i - 1));
        m_pDailyloglist[i]->setTodayLog(jsonData.logOfTheDay(iWeekNum, i));
    }
}

void MorningPaper::on_btnsave_clicked()
{
    QVector<QString> vecLogOfDay(DAILYNUM);
    JsonData jsonData;
    QDateTime dateTime;

    int iWeekNum = dateTime.currentDateTime().date().weekNumber();

    for (int i = 0; i < DAILYNUM; i++) {
        vecLogOfDay[i] = m_pDailyloglist[i]->getTodayLog();
    }
    if (jsonData.writeData(iWeekNum, vecLogOfDay) != -1) {
        QMessageBox::information(this, "save", "save success", QMessageBox::Ok);
    }
    return;
}
