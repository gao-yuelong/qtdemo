#include "graphsimple.h"
#include "ui_graphsimple.h"
#include "customscene.h"

#include <QListWidgetItem>
#include <QDebug>

GraphSimple::GraphSimple(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphSimple)
{
    ui->setupUi(this);

    CustomScene *pCustomScene = new CustomScene();
    ui->graphicsView->setScene(pCustomScene);
    ui->graphicsView->setSceneRect(ui->graphicsView->rect());

    QListWidgetItem *pItemBoy = new QListWidgetItem(QIcon(":/images/boy.png"), "boy");
    QListWidgetItem *pItemGirl = new QListWidgetItem(QIcon(":/images/girl.png"), "girl");
    QListWidgetItem *pItemMen = new QListWidgetItem(QIcon(":/images/men.png"), "men");
    QListWidgetItem *pItemWomen = new QListWidgetItem(QIcon(":/images/women.png"), "women");

    ui->listWidget->setDragEnabled(true);
    ui->listWidget->addItem(pItemBoy);
    ui->listWidget->addItem(pItemGirl);
    ui->listWidget->addItem(pItemMen);
    ui->listWidget->addItem(pItemWomen);
}

GraphSimple::~GraphSimple()
{
    delete ui;
}
