#ifndef GRAPHSIMPLE_H
#define GRAPHSIMPLE_H

#include <QWidget>

namespace Ui {
class GraphSimple;
}

class GraphSimple : public QWidget
{
    Q_OBJECT

public:
    explicit GraphSimple(QWidget *parent = nullptr);
    ~GraphSimple();

private:
    Ui::GraphSimple *ui;
};

#endif // GRAPHSIMPLE_H
