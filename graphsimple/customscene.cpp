#include "customscene.h"
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QListWidget>
#include <QGraphicsPixmapItem>
#include <QDebug>

CustomScene::CustomScene()
{

}

void CustomScene::dragEnterEvent(QGraphicsSceneDragDropEvent *)
{
    qDebug() << __FUNCTION__;
}

void CustomScene::dragLeaveEvent(QGraphicsSceneDragDropEvent *)
{
    qDebug() << __FUNCTION__;
}

void CustomScene::dragMoveEvent(QGraphicsSceneDragDropEvent *)
{
    qDebug() << __FUNCTION__;
}

void CustomScene::dropEvent(QGraphicsSceneDragDropEvent *pEvent)
{
    qDebug() << __FUNCTION__;

    if (pEvent->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist"))
    {
        QListWidget *pListwidget = qobject_cast<QListWidget *>(pEvent->source());

        QString strPixmapPath = ":/images/" + pListwidget->currentItem()->text() + ".png";
        QGraphicsPixmapItem *pPixmapItem = new QGraphicsPixmapItem(QPixmap(strPixmapPath));

        pPixmapItem->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
        pPixmapItem->setPos(pEvent->scenePos());

        addItem(pPixmapItem);
    }
}
