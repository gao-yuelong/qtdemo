#ifndef LISTMODEL_H
#define LISTMODEL_H

#include <QAbstractListModel>
#include "listitem.h"

class ListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)

public:
    explicit ListModel(ListItem* prototype, QObject *parent = nullptr);

    ~ListModel();

    int rowCount(
        const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex& index,int role) const Q_DECL_OVERRIDE;

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    void appendRow(ListItem* item);

    void appendRows(QList<ListItem*>& items);

    void insertRow(int row, ListItem* item);

    bool removeRow(int row, const QModelIndex& index = QModelIndex());

    bool removeRows(int row, int count, const QModelIndex& index = QModelIndex()) Q_DECL_OVERRIDE;

    ListItem* find(QString itemId) const;
    ListItem* find(int itemId) const;

    //ListItem* find(int itemId) const;

    ListItem* at(int index) const;

    int getRowFromItem(ListItem* item) const;

    QModelIndex indexFromItem(ListItem* item) const;

    QList<ListItem*> to_QList() const;

    Q_INVOKABLE QVariant get(
        int index);

    Q_INVOKABLE int rowIndexFromId(
        int itemId);

//    Q_INVOKABLE int rowIndexFromId(
//        backend::EntityId itemId);

    Q_INVOKABLE void clear();

protected:
    ListItem* prototype_;
    QList<ListItem*> items_;

private slots:
    void updateItem();

signals:
    void countChanged(
        int count);
};

#endif // LISTMODEL_H
