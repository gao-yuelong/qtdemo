#ifndef LOCATORMODELITEM_H
#define LOCATORMODELITEM_H

#include "listitem.h"

class LocatorModelItem : public ListItem
{
    Q_OBJECT

public:

    // Use ListItem constructors
    using ListItem::ListItem;

    //! Overwriter entity kind
    virtual EntityKind backend_kind() const override
    {
        return EntityKind::LOCATOR;
    }

};

#endif // LOCATORMODELITEM_H
