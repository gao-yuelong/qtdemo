#ifndef LISTITEM_H
#define LISTITEM_H

#include <QObject>
#include "common.h"

class ListItem : public QObject
{
    Q_OBJECT
public:

    enum ModelItemRoles{
        idRole = Qt::UserRole + 1,
        kindRole,
        aliveRole,
        metatrafficRole,
        clickedRole,
        nameRole
    };

    //constructor
    ListItem(QObject *parent = 0);
    ListItem(int id, QObject *parent = 0);
    ListItem(int id, EntityInfo info, QObject *parent = 0);

    ~ListItem();

    virtual QString entity_id() const;

    virtual QString name() const;

    virtual EntityInfo info() const;

    virtual QString kind() const;

    virtual EntityKind backend_kind() const
    {
        return EntityKind::INVALID;
    }

    virtual bool alive() const;
    virtual bool metatraffic() const;
    virtual bool clicked() const;

    void info(
        EntityInfo info)
    {
        info_ = info;
    }

    void clicked(
        bool clicked)
    {
        clicked_ = clicked;
    }

    int get_entity_id() const;

    virtual QVariant data(
        int role) const;

    virtual QHash<int, QByteArray> roleNames() const;

    virtual void triggerItemUpdate()
    {
        emit dataChanged();
    }

signals:

    //! Communicate to the view that some internal info has been modified/added/removed/updated
    void dataChanged();

protected:

    //! Backend Id that references the \c Entity that this Item represents
    int id_;

    //! Backend info that contains all the needed information for this item
    EntityInfo info_;

    //! States whether the entity is clicked or not
    bool clicked_;
};

#endif // LISTITEM_H
