#include "listitem.h"

#include <QVariant>

ListItem::ListItem(
    QObject* parent)
    : QObject(parent)
    , id_(-1)
{
}

ListItem::ListItem(
    int id,
    QObject* parent)
    : QObject(parent)
    , id_(id)
{
}

ListItem::ListItem(
    int id,
    EntityInfo info,
    QObject* parent)
    : QObject(parent)
    , id_(id)
    , info_(info)
    , clicked_(false)
{
}

ListItem::~ListItem()
{
}

QString ListItem::entity_id() const
{
    return "2"; //backend::backend_id_to_models_id(id_);
}

QString ListItem::name() const
{
    return "listItem";
}

QString ListItem::kind() const
{
    return "kind";
}

bool ListItem::alive() const
{
    return true;
}

bool ListItem::metatraffic() const
{
    return true;
}

bool ListItem::clicked() const
{
    return clicked_;
}

EntityInfo ListItem::info() const
{
    return info_;
}

int ListItem::get_entity_id() const
{
    return id_;
}

QVariant ListItem::data(
    int role) const
{
    switch(role) {
    case Qt::DisplayRole:
        return this->name();
    }

    return QVariant();
//    switch (role)
//    {
//    case idRole:
//        return this->entity_id();
//    case nameRole:
//        return this->name();
//    case kindRole:
//        return this->kind();
//    case aliveRole:
//        return this->alive();
//    case metatrafficRole:
//        return this->metatraffic();
//    case clickedRole:
//        return this->clicked();
//    default:
//        return QVariant();
//    }
}

QHash<int, QByteArray> ListItem::roleNames() const
{
    QHash<int, QByteArray>  roles;

    roles[idRole] = "id";
    roles[nameRole] = "name";
    roles[kindRole] = "kind";
    roles[aliveRole] = "alive";
    roles[metatrafficRole] = "metatraffic";
    roles[clickedRole] = "clicked";

    return roles;
}
