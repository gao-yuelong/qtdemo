#ifndef SUBLISTEDLISTMODEL_H
#define SUBLISTEDLISTMODEL_H

#include "listmodel.h"
#include "sublistedlistitem.h"

class SubListedListModel : public ListModel
{
    Q_OBJECT
public:
    explicit SubListedListModel(
        SubListedListItem* prototype,
        QObject* parent = 0);

    /**
     * Returns the model contained by row item identified by a given item id.
     */
    Q_INVOKABLE QObject* subModelFromEntityId(
        QString entity_id);
};

#endif // SUBLISTEDLISTMODEL_H
