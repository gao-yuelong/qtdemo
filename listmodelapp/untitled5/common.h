#ifndef COMMON_H
#define COMMON_H

#include <QString>

enum class EntityKind
{
    /// Invalid entity kind
    INVALID,

    /// Host/Machine where a participant is allocated
    HOST,

    /// User that has executed a participant
    USER,

    /// Process where a participant is running
    PROCESS,

    /// Abstract DDS network by Domain or by Discovery Server
    DOMAIN_,

    /// DDS Topic
    TOPIC,

    /// DDS Domain Participant
    PARTICIPANT,

    /// DDS DataWriter
    DATAWRITER,

    /// DDS DataReader
    DATAREADER,

    /// Physical locator that a communication is using (IP + port || SHM + port)
    /// Store the Locator Statistic data
    LOCATOR,
};

struct EntityInfo {
    QString info;
};

#endif // COMMON_H
