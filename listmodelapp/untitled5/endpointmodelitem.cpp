#include "endpointmodelitem.h"

QVariant EndpointModelItem::data(
    int role) const
{
    switch(role) {
    case Qt::DisplayRole:
        return this->name();
    }

    return QVariant();
//    switch (role)
//    {
//    case idRole:
//        return this->entity_id();
//    case nameRole:
//        return this->name();
//    case kindRole:
//        return this->kind();
//    case guidRole:
//        return this->guid();
//    case topicRole:
//        return this->topic();
//    case aliveRole:
//        return this->alive();
//    case clickedRole:
//        return this->clicked();
//    default:
//        return QVariant();
//    }
}

QString EndpointModelItem::guid() const
{
    return "EndpointModelItem::guid()";
}

QString EndpointModelItem::topic() const
{
    return "EndpointModelItem::topic()";
}

QHash<int, QByteArray> EndpointModelItem::roleNames() const
{
    QHash<int, QByteArray>  roles = ListItem::roleNames();

    roles[guidRole] = "guid";
    roles[topicRole] = "topic";

    return roles;
}

