#include "sublistedlistmodel.h"

SubListedListModel::SubListedListModel(
    SubListedListItem* prototype,
    QObject* parent)
    : ListModel(prototype, parent)
{
}

QObject* SubListedListModel::subModelFromEntityId(
    QString entity_id)
{
    SubListedListItem* item = (SubListedListItem*)find(entity_id);
    if (item != nullptr)
    {
        return item->submodel();
    }
    return nullptr;
}
