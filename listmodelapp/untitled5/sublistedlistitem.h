#ifndef SUBLISTEDLISTITEM_H
#define SUBLISTEDLISTITEM_H

#include "listitem.h"
#include "listmodel.h"

class SubListedListItem : public ListItem
{
    Q_OBJECT
public:
    using ListItem::ListItem;

    //! Clear and delete the subentities
    ~SubListedListItem()
    {
        if (subentities_list_model_)
        {
            subentities_list_model_->clear();
            delete subentities_list_model_;
        }
    }

    //! Return the submodel that contains the subentities of this item
    ListModel* submodel()  const
    {
        return subentities_list_model_;
    }

protected:

    //! Submodule with subentities
    ListModel* subentities_list_model_;
};

#endif // SUBLISTEDLISTITEM_H
