#ifndef ENDPOINTMODELITEM_H
#define ENDPOINTMODELITEM_H

#include <QObject>
#include "locatormodelitem.h"
#include "listmodel.h"
#include "EntityContainerModelItem.h"

class EndpointModelItem : public EntityContainerModelItem<LocatorModelItem, ListModel>
{
    Q_OBJECT

public:

    //! Add new roles only for Endpoint items
    enum EndpointModelItemRoles
    {
        guidRole = ModelItemRoles::nameRole + 1,    //! Role for attribute GUID
        topicRole                                   //! Role for attribute Topic
    };

    // EntityContainerModelItem default constructor
    EndpointModelItem(
        QObject* parent = 0)
        : EntityContainerModelItem(parent)
    {
    }

    // EntityContainerModelItem constructor
    EndpointModelItem(
        int id,
        EntityInfo info,
        QObject* parent = 0)
        : EntityContainerModelItem(id, info, parent)
    {
    }

    //! Specific DDS Item constructor, with a backend \c EntityId associated
    EndpointModelItem(
        int id,
        EntityInfo info,
        EntityKind kind,
        QObject* parent = 0)
        : EntityContainerModelItem(id, info, parent)
        , kind_(kind)
    {
    }

    //! Override the ListItem \c data method to add new roles
    QVariant data(
        int role) const override;

    //! Getter for guid attribute
    QString guid() const;

    //! Getter for topic attribute
    QString topic() const;

    //! Override the ListItem \c roleNames method to add new roles
    QHash<int, QByteArray> roleNames() const override;

    //! Overwriter entity kind
    virtual EntityKind backend_kind() const override
    {
        return kind_;
    }

protected:

    //! Wether the entity is DataWriter or DataReader
    EntityKind kind_;
};

#endif // ENDPOINTMODELITEM_H
