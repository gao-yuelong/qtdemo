#ifndef ENTITYCONTAINERMODELITEM_H
#define ENTITYCONTAINERMODELITEM_H

#include "sublistedlistitem.h"
#include "common.h"

template<class SubEntityClass, class SubEntityModelClass>
class EntityContainerModelItem : public SubListedListItem
{
public:

    //! QObject defult constructor that creates the submodule by the template classes
    EntityContainerModelItem(
        QObject* parent = 0)
        : SubListedListItem(parent)
    {
        subentities_list_model_ = new SubEntityModelClass(new SubEntityClass());
    }

    //! Item constructor that creates the submodule by the template classes and get an \c EntityId reference to entity
    EntityContainerModelItem(
        int id,
        EntityInfo info,
        QObject* parent = 0)
        : SubListedListItem(id, info, parent)
    {
        subentities_list_model_ = new SubEntityModelClass(new SubEntityClass());
    }

};

#endif // ENTITYCONTAINERMODELITEM_H
