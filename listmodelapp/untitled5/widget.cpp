#include "widget.h"
#include "ui_widget.h"

#include "listmodel.h"
#include "sublistedlistmodel.h"
#include "participantmodelitem.h"
#include "endpointmodelitem.h"
#include "common.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    ListModel* participants_model_ = new SubListedListModel(new ParticipantModelItem());

    EntityInfo info;
    info.info = "123";
    ListItem* item = new ParticipantModelItem(0, info);
    //item->clicked(last_clicked);
    participants_model_->appendRow(item);

    ListItem *item1 = new EndpointModelItem(1, info);

    participants_model_->appendRow(item1);

    ui->listView->setModel(participants_model_);
}

Widget::~Widget()
{
    delete ui;
}

