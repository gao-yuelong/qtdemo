QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    endpointmodelitem.cpp \
    listitem.cpp \
    listmodel.cpp \
    locatormodelitem.cpp \
    main.cpp \
    participantmodelitem.cpp \
    sublistedlistitem.cpp \
    sublistedlistmodel.cpp \
    widget.cpp

HEADERS += \
    EntityContainerModelItem.h \
    common.h \
    endpointmodelitem.h \
    listitem.h \
    listmodel.h \
    locatormodelitem.h \
    participantmodelitem.h \
    sublistedlistitem.h \
    sublistedlistmodel.h \
    widget.h

FORMS += \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
