#include "listmodel.h"

ListModel::ListModel(
    ListItem* prototype,
    QObject* parent)
    : QAbstractListModel(parent)
{
    //QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
    prototype_ = prototype;
    items_ = QList<ListItem*>();
}

ListModel::~ListModel()
{
    delete prototype_;
}

int ListModel::rowCount(
    const QModelIndex&) const
{
    return items_.size();
}
#include <QDebug>>
QVariant ListModel::data(
    const QModelIndex& index,
    int role) const
{
    //qDebug() << "role: " << role;
    if (index.row() >= 0 && index.row() < items_.size())
    {
        return items_.at(index.row())->data(role);
    }
    return QVariant();
}

QHash<int, QByteArray> ListModel::roleNames() const
{
    return prototype_->roleNames();
}

void ListModel::appendRow(
    ListItem* item)
{
    if (item != nullptr)
    {
        appendRows(QList<ListItem*>() << item);
        emit countChanged(rowCount());
    }
}

void ListModel::appendRows(
    QList<ListItem*>& items)
{
    if (items.size() == 0)
    {
        return;
    }

    beginInsertRows(QModelIndex(), rowCount(), rowCount() + items.size() - 1);
    foreach(ListItem * item, items)
    {
        QObject::connect(item, SIGNAL(dataChanged()), this, SLOT(updateItem()));
        items_.append(item);
    }
    endInsertRows();

    emit countChanged(rowCount());
}

void ListModel::insertRow(
    int row,
    ListItem* item)
{
    if (item == nullptr)
    {
        return;
    }

    beginInsertRows(QModelIndex(), row, row);
    QObject::connect(item, SIGNAL(dataChanged()), this, SLOT(updateItem()));
    items_.insert(row, item);
    endInsertRows();
    emit countChanged(rowCount());
}

bool ListModel::removeRow(
    int row,
    const QModelIndex& index)
{
    if (row >= 0 && row < items_.size())
    {
        beginRemoveRows(index, row, row);
        ListItem* item = items_.takeAt(row);
        delete item;
        endRemoveRows();
        emit countChanged(rowCount());
        return true;
    }
    return false;
}

bool ListModel::removeRows(
    int row,
    int count,
    const QModelIndex& index)
{
    if (row >= 0 && count > 0 && (row + count) <= items_.size())
    {
        beginRemoveRows(index, row, row + count - 1);
        for (int i = 0; i < count; i++)
        {
            ListItem* item = items_.takeAt(row);
            delete item;
            item = nullptr;
        }
        endRemoveRows();
        emit countChanged(rowCount());
        return true;
    }
    return false;
}

void ListModel::clear()
{
    if (items_.size() == 0)
    {
        return;
    }
    removeRows(0, items_.size());
    emit countChanged(rowCount());
}

QModelIndex ListModel::indexFromItem(
    ListItem* item) const
{
    if (item != nullptr)
    {
        for (int i = 0; i < items_.size(); i++)
        {
            if (items_.at(i) == item)
            {
                return index(i);
            }
        }
    }
    return QModelIndex();
}

ListItem* ListModel::find(
    QString itemId) const
{
    foreach(ListItem * item, items_)
    {
        if (item->entity_id() == itemId)
        {
            return item;
        }
    }
    return nullptr;
}

ListItem* ListModel::find(
    int itemId) const
{
    foreach(ListItem * item, items_)
    {
        if (item->entity_id() == itemId)
        {
            return item;
        }
    }
    return nullptr;
}

//ListItem* ListModel::find(
//    backend::EntityId itemId) const
//{
//    foreach(ListItem * item, items_)
//    {
//        if (item->get_entity_id() == itemId)
//        {
//            return item;
//        }
//    }
//    return nullptr;
//}

ListItem* ListModel::at(
    int index) const
{
    return items_.at(index);
}

int ListModel::getRowFromItem(
    ListItem* item) const
{
    if (item != nullptr)
    {
        for (int i = 0; i < items_.size(); i++)
        {
            if (items_.at(i) == item)
            {
                return i;
            }
        }
    }
    return -1;
}

QList<ListItem*> ListModel::to_QList() const
{
    return items_;
}

void ListModel::updateItem()
{
    ListItem* item = static_cast<ListItem*>(sender());
    QModelIndex index = indexFromItem(item);
    if (index.isValid())
    {
        emit dataChanged(index, index);
    }
}

QVariant ListModel::get(
    int index)
{
    if (index >= items_.size() || index < 0)
    {
        return QVariant();
    }
    ListItem* item = items_.at(index);
    QMap<QString, QVariant> itemData;
    QHashIterator<int, QByteArray> hashItr(item->roleNames());

    while (hashItr.hasNext())
    {
        hashItr.next();
        itemData.insert(hashItr.value(), QVariant(item->data(hashItr.key())));
    }
    return QVariant(itemData);
}

int ListModel::rowIndexFromId(
    int id)
{
    ListItem* item = find(id);

    if (item)
    {
        return indexFromItem(item).row();
    }
    return -1;
}

//int ListModel::rowIndexFromId(
//    backend::EntityId id)
//{
//    ListItem* item = find(id);

//    if (item)
//    {
//        return indexFromItem(item).row();
//    }
//    return -1;
//}
