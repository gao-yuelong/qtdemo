#include "participantmodelitem.h"
#include <QDebug>
QVariant ParticipantModelItem::data(
    int role) const
{
    switch(role) {
    case Qt::DisplayRole:
        return this->name();
    }

    return QVariant();
//    qDebug() << "part role" << role;
//    switch (role)
//    {
//    case idRole:
//        return this->entity_id();
//    case nameRole:
//        return this->name();
//    case kindRole:
//        return this->kind();
//    case guidRole:
//        return this->guid();
//    case domainRole:
//        return this->domain();
//    case aliveRole:
//        return this->alive();
//    case clickedRole:
//        return this->clicked();
//    default:
//        return QVariant();
//    }
}

QString ParticipantModelItem::guid() const
{
    return "ParticipantModelItem::guid()";
}

QString ParticipantModelItem::domain() const
{
    return "ParticipantModelItem::domain()";
}

QHash<int, QByteArray> ParticipantModelItem::roleNames() const
{
    QHash<int, QByteArray>  roles = ListItem::roleNames();

    roles[guidRole] = "guid";
    roles[domainRole] = "domain";

    return roles;
}
