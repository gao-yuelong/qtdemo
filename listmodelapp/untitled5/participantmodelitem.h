#ifndef PARTICIPANTMODELITEM_H
#define PARTICIPANTMODELITEM_H

#include <QObject>
#include "EntityContainerModelItem.h"
#include "endpointmodelitem.h"
#include "sublistedlistmodel.h"

class ParticipantModelItem : public EntityContainerModelItem<EndpointModelItem, SubListedListModel>
{
    Q_OBJECT

public:

    //! Add new roles only for Participant items
    enum ParticipantModelItemRoles
    {
        guidRole = ModelItemRoles::nameRole + 1,    //! Role for attribute GUID
        domainRole                                  //! Role for attribute Domain
    };

    //! Default ListItem constructor
    using EntityContainerModelItem::EntityContainerModelItem;

    //! Override the ListItem \c data method to add new roles
    QVariant data(
        int role) const override;

    //! Getter for guid attribute
    QString guid() const;

    //! Getter for domain attribute
    QString domain() const;

    //! Override the ListItem \c roleNames method to add new roles
    QHash<int, QByteArray> roleNames() const override;

    //! Overwriter entity kind
    virtual EntityKind backend_kind() const override
    {
        return EntityKind::PARTICIPANT;
    }

};

#endif // PARTICIPANTMODELITEM_H
