#include <QPixmap>
#include <QDebug>
#include <QTimer>
#include <QRandomGenerator>
#include "widget.h"
#include "ui_widget.h"

const int MAX_PRIZE_NUMBER = 9;

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    m_iCurWidget(1),
    m_iTimeoutNumber(0),
    m_iRandomTimes(2),
    m_iPrizeNumber(9)
{
    ui->setupUi(this);

    m_pTimer = new QTimer(this);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(refresh()));

    init();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::init()
{
    m_hash[1] = ui->label1;
    m_hash[2] = ui->label2;
    m_hash[3] = ui->label3;
    m_hash[4] = ui->label4;
    m_hash[5] = ui->label5;
    m_hash[6] = ui->label6;
    m_hash[7] = ui->label7;
    m_hash[8] = ui->label8;
    m_hash[9] = ui->label9;
    for (auto hash : m_hash) {
        hash->setVisible(false);
    }
}

void Widget::refresh()
{
    //qDebug() << "timeout";
    if (m_iCurWidget > 9) {
        m_iCurWidget = 1;
    }

    //清空样式
    clearWidgetStyle();
    QString strStyle = QString("#widget%1{\nborder:2px solid red;\n}").arg(m_iCurWidget);
    setStyleSheet(strStyle);
    //前面几轮转完了，同时到达了随机序号的位置，暂停
    if ((m_iTimeoutNumber == MAX_PRIZE_NUMBER * m_iRandomTimes) && m_iCurRandom == m_iCurWidget)
    {
        m_iTimeoutNumber = 0;
        m_pTimer->stop();
        m_hash[m_iCurWidget]->setVisible(true);
    }
    m_iCurWidget++;
    if (m_iTimeoutNumber < MAX_PRIZE_NUMBER * m_iRandomTimes) m_iTimeoutNumber++;

}

void Widget::clearWidgetStyle()
{
    setStyleSheet("");
    for (auto hash : m_hash) {
        hash->setVisible(false);
    }
}

void Widget::selectAnimation()
{
    //开启定时器
    m_pTimer->start(100);
    m_iCurWidget = 1;
}

void Widget::on_btnOk_clicked()
{
    //产生随机数
    //m_iCurRandom = QRandomGenerator::global()->bounded(9) + 1;

    if (m_iPrizeNumber == 0) {
        m_iPrizeNumber = 9;
        m_hashMap.clear();
    }
    m_iCurRandom = customRandom();

    //产生随机数，决定开始跑几轮
    m_iRandomTimes = QRandomGenerator::global()->bounded(2) + 1;

    selectAnimation();
}

int Widget::customRandom()
{
    int iReturn;
    int iRandom = QRandomGenerator::global()->bounded(m_iPrizeNumber) + 1;

    //返回未产生过的随机值
    if (m_hashMap.count(iRandom)) {
        iReturn = m_hashMap[iRandom];
    } else {
        iReturn = iRandom;
    }

    //建立映射关系
    if (m_hashMap.count(m_iPrizeNumber)) {
        m_hashMap[iRandom] = m_hashMap[m_iPrizeNumber];
    } else {
        m_hashMap[iRandom] = m_iPrizeNumber;
    }

    //范围不断缩小
    m_iPrizeNumber--;
    return iReturn;
}
