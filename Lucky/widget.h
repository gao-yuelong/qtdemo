#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLabel>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

    void selectAnimation(); //开始动画
    void init();            //初始化
    int customRandom();     //返回随机值

private slots:
    void on_btnOk_clicked();
    void refresh();
    void clearWidgetStyle();

private:
    Ui::Widget *ui;
    QTimer *m_pTimer;
    int m_iCurWidget;       //当前所在widget，主要用来加边框的
    int m_iTimeoutNumber;   //定时器触发次数
    int m_iCurRandom;       //当前产生的随机值
    int m_iRandomTimes;     //开始的时候转几轮

    QHash<int, QLabel *> m_hash;    //建立int和QLabel的映射关系
    QHash<int, int> m_hashMap;      //在产生随机数时建立映射关系
    int m_iPrizeNumber;     //奖品序号
};

#endif // WIDGET_H
