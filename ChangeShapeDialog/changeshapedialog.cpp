﻿//c++ gui qt4编程 可变形状的dialog
#include "changeshapedialog.h"
#include "ui_changeshapedialog.h"

ChangeShapeDialog::ChangeShapeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangeShapeDialog)
{
    ui->setupUi(this);

    ui->secondaryGroupBox->hide();
    ui->tertiaryGroupBox->hide();

    /*
    构造函数隐藏了对话框的第二键和第三键这两个部分。它也把有关窗体布局的sizeConstraint
    属性设置为QLayout::SetFixedSize，这样会使用户不能再重新修改这个对话框窗体的大小。这样一来，
    布局就会负责对话框重新定义大小的职责，并且也会在显示或隐藏子窗口部件的时候自动重新定义这个对话框
    的大小，从而可以确保对话框总是能以最佳的尺寸显示出来。
    */

    layout()->setSizeConstraint(QLayout::SetFixedSize);

    setColumnRange('A', 'Z');
}

ChangeShapeDialog::~ChangeShapeDialog()
{
    delete ui;
}

void ChangeShapeDialog::setColumnRange(QChar first, QChar last)
{
    ui->primaryColumnCombo->clear();
    ui->secondaryColumnCombo->clear();
    ui->tertiaryColumnCombo->clear();

    ui->secondaryColumnCombo->addItem(tr("None"));
    ui->tertiaryColumnCombo->addItem(tr("None"));

    /*
    QWidget::sizeHint()函数可以返回布局系统试图认同的“理想”大小。这也解释了为什么不同的窗口部件
    或者具有不同内容的类似窗口部件通常会被布局系统分配给不同的大小尺寸。对于这些组合框，这里指的是
    第二键组合框和第三组合框，由于它们包含了一个”None“选项，所以它们要比只包含了一个单字符项目的
    主键组合框显的宽一些。为了避免这种不一致性，需要把主键组合框的最小大小设置成第二键组合框的理想大小。
    */

    ui->primaryColumnCombo->setMinimumSize(ui->secondaryColumnCombo->sizeHint());

    QChar ch = first;
    while (ch <= last) {
        ui->primaryColumnCombo->addItem(QString(ch));
        ui->secondaryColumnCombo->addItem(QString(ch));
        ui->tertiaryColumnCombo->addItem(QString(ch));
        ch = ch.unicode() + 1;
    }
}
