﻿#ifndef CHANGESHAPEDIALOG_H
#define CHANGESHAPEDIALOG_H

#include <QDialog>

namespace Ui {
class ChangeShapeDialog;
}

class ChangeShapeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChangeShapeDialog(QWidget *parent = nullptr);
    ~ChangeShapeDialog();
    void setColumnRange(QChar first, QChar last);

private:
    Ui::ChangeShapeDialog *ui;
};

#endif // CHANGESHAPEDIALOG_H
